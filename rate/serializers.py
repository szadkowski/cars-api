from rest_framework import serializers

from .models import Rate


class RateSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        return Rate.objects.create_rate(**validated_data)

    class Meta:
        model = Rate
        fields = '__all__'
