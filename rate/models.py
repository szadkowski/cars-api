from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from car.models import Car


class RateManager(models.Manager):
    use_in_migration = True

    def create_rate(self, car_id, rating):
        if not car_id:
            raise ValueError("car_id must have value")
        if not rating:
            raise ValueError("rating must have value")

        rate = self.model()
        rate.car_id = car_id
        rate.rating = rating

        rate.save()
        return rate


class Rate(models.Model):
    id = models.AutoField(primary_key=True)
    car_id = models.ForeignKey(Car, on_delete=models.CASCADE, related_name='rates')
    rating = models.IntegerField(validators=[MaxValueValidator(5), MinValueValidator(1)])

    REQUIRED_FIELDS = ["car_id", "rating"]
    objects = RateManager()

    def get_id(self):
        return self.id

    def get_car_id(self):
        return self.car_id.get_id()

    def get_rating(self):
        return self.rating
