import json

from django.http import HttpResponse, JsonResponse
from rest_framework.views import APIView, status

from rate.serializers import RateSerializer


class RateView(APIView):

    @staticmethod
    def post(request):
        request_body = json.loads(request.body)
        try:
            request_car = request_body["car_id"]
        except KeyError:
            return HttpResponse(status=status.HTTP_406_NOT_ACCEPTABLE)

        serializer = RateSerializer(data=request_body)
        if serializer.is_valid():
            rate = serializer.save()
            if rate:
                rate.save()
                return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
