from django.db.models import Avg
from rest_framework import serializers

from rate.models import Rate
from .models import Car


class CarSerializer(serializers.ModelSerializer):
    avg_rating = serializers.SerializerMethodField()

    def create(self, validated_data):
        return Car.objects.create_car(**validated_data)

    def get_avg_rating(self, obj):
        return obj.rates.aggregate(avg_rating=Avg('rating'))['avg_rating']

    class Meta:
        model = Car
        fields = ("id", "make", "model", "avg_rating")
        read_only_fields = ("id", "avg_rating")


class PopularCarSerializer(serializers.ModelSerializer):
    rates_number = serializers.SerializerMethodField(read_only=True)

    def get_rates_number(self, obj):
        return obj.rates.count()

    class Meta:
        model = Car
        fields = ("id", "make", "model", "rates_number")
