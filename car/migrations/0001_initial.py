# Generated by Django 3.1.7 on 2021-03-05 11:57

import car.models
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('car_id', models.AutoField(primary_key=True, serialize=False)),
                ('make', models.CharField(max_length=100)),
                ('model', models.CharField(max_length=100)),
                ('avg_rating', models.FloatField(default=0, validators=[django.core.validators.MaxValueValidator(5.0), django.core.validators.MinValueValidator(0.0)])),
                ('rates_number', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)])),
            ],
            managers=[
                ('objects', car.models.CarManager()),
            ],
        ),
    ]
