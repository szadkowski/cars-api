import json
import os
from heapq import nlargest

from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, HttpResponse
from rest_framework.views import APIView, status
import requests

from car.models import Car
from car.serializers import CarSerializer, PopularCarSerializer

MAIN_DOMAIN = 'https://vpic.nhtsa.dot.gov/api/'
MAKES_ENDPOINT = 'vehicles/GetAllMakes?format=json'
MODELS_ENDPOINT = 'vehicles/GetModelsForMakeId/{}?format=json'


class CarView(APIView):
    @staticmethod
    def get(request):
        cars = Car.objects.all()
        serializer = CarSerializer(cars, many=True)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK, safe=False)

    @staticmethod
    def post(request):
        request_body = json.loads(request.body)
        request_make = request_body["make"]
        request_model = request_body["model"]

        response = requests.get(os.path.join(MAIN_DOMAIN, MAKES_ENDPOINT))
        if response.status_code == 200:
            response_body = json.loads(response.content)
            makes = response_body["Results"]
            matched_makes = list(filter(lambda make: make["Make_Name"].casefold() == request_make.casefold(), makes))
            if len(matched_makes) != 1:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
            make_id = matched_makes[0]["Make_ID"]

            response = requests.get(os.path.join(MAIN_DOMAIN, MODELS_ENDPOINT)
                                    .format(make_id))
            if response.status_code == 200:
                response_body = json.loads(response.content)
                models = response_body["Results"]
                matched_models = list(filter(lambda model: model["Model_Name"].casefold() == request_model.casefold(),
                                             models))
                if len(matched_models) != 1:
                    return HttpResponse(status=status.HTTP_404_NOT_FOUND)
            else:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        else:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        serializer = CarSerializer(data=request_body)
        if serializer.is_valid():
            car = serializer.save()
            if car:
                car.save()
                return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    @staticmethod
    def delete(request, car_id=None):
        try:
            car = Car.objects.get(id=car_id)
        except ObjectDoesNotExist:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        car.delete()
        return HttpResponse(status=status.HTTP_200_OK)


class PopularView(APIView):

    def get(self, request):
        cars = Car.objects.all()
        serializer = PopularCarSerializer(cars, many=True)

        most_popular = nlargest(2, serializer.data, key=lambda car: car["rates_number"])
        return JsonResponse(most_popular, status=status.HTTP_200_OK, safe=False)
