from django.db import models


class CarManager(models.Manager):
    use_in_migrations = True

    def create_car(self, make, model):
        if not make:
            raise ValueError("make must have value")
        if not model:
            raise ValueError("model must have value")

        car = self.model()
        car.make = make
        car.model = model

        car.save()
        return car


class Car(models.Model):
    id = models.AutoField(primary_key=True)
    make = models.CharField(max_length=100, blank=False)
    model = models.CharField(max_length=100, blank=False)

    REQUIRED_FIELDS = ["make", "model"]
    objects = CarManager()

    def get_id(self):
        return self.id

    def get_make(self):
        return self.make

    def get_model(self):
        return self.model
