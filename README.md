### Dev

To run locally:

```sh
$ docker build -t web:latest .
$ docker run -d --name django-netguru -e "PORT=8765" -e "DEBUG=1" -p 8000:8765 web:latest
