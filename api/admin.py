from django.contrib import admin

from car.models import Car


class CarAdmin(admin.ModelAdmin):
    list_display = ("car_id", "make", "mode", "avg_rating", "rates_number")


admin.site.register(Car, CarAdmin)
