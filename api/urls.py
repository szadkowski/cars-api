from django.contrib import admin
from django.urls import path, re_path

from car.views import CarView, PopularView
from rate.views import RateView
from .views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^cars/(?:(?P<car_id>\d+)/)?$', CarView.as_view(), name="cars"),
    path("home/", home, name="home"),
    path("rate/", RateView.as_view(), name="rates"),
    path("popular/", PopularView.as_view(), name="popular")
]
