import json

from django.test import Client, TestCase

from car.models import Car
from rate.models import Rate


class TestRates(TestCase):

    def setUp(self):
        self.aston = Car.objects.create_car(
            make="Aston Martin", model="v8"
        )
        self.golf = Car.objects.create_car(
            make="Volkswagen", model="golf"
        )
        self.tesla = Car.objects.create_car(
            make="Tesla", model="Roadster"
        )
        Rate.objects.create_rate(car_id=self.aston, rating=1)
        Rate.objects.create_rate(car_id=self.aston, rating=3)
        Rate.objects.create_rate(car_id=self.aston, rating=5)
        Rate.objects.create_rate(car_id=self.golf, rating=2)
        Rate.objects.create_rate(car_id=self.golf, rating=3)
        Rate.objects.create_rate(car_id=self.tesla, rating=5)

    def test_post_rate_succeeds(self):
        response = self.client.post(
            '/rate/',
            json.dumps({"car_id": "1", "rating": "1"}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)

    def test_post_rate_fails(self):
        response = self.client.post(
            '/rate/',
            json.dumps({"car_id": "4", "rating": "1"}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 404)

        response = self.client.post(
            '/rate/',
            json.dumps({"car_id": "1", "rating": "6"}),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 404)

    def test_popular(self):
        response = self.client.get(
            '/popular/',
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)
        cars = json.loads(response.content)
        first = cars[0]
        second = cars[1]
        self.assertEqual(first["rates_number"], 3)
        self.assertEqual(second["rates_number"], 2)

    def test_ratings(self):
        response = self.client.get(
            '/cars/',
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)
        cars = json.loads(response.content)
        first = cars[0]
        second = cars[1]
        third = cars[2]
        self.assertEqual(first["avg_rating"], 3.0)
        self.assertEqual(second["avg_rating"], 2.5)
        self.assertEqual(third["avg_rating"], 5.0)
