import json

from django.test import Client, TestCase

from car.models import Car


class TestCars(TestCase):

    def setUp(self):
        self.aston = Car.objects.create_car(
            make="Aston Martin", model="v8"
        )
        self.golf = Car.objects.create_car(
            make="Volkswagen", model="golf"
        )

    def test_car_creation_succeeds(self):
        response = self.client.post(
            '/cars/',
            json.dumps({"make": "Aston Martin", "model": "Db11"}),
            content_type='application/json'
        )
        # has status 201 Created
        self.assertEqual(response.status_code, 201)
        car = Car.objects.get(id=3)
        self.assertEqual(car.make, "Aston Martin")
        self.assertEqual(car.model, "Db11")

    def test_car_creation_fails(self):
        response = self.client.post(
            '/cars/',
            json.dumps({"make": "Aston Martin", "model": "xxxxx"}),
            content_type='application/json'
        )
        # has status 201 Created
        self.assertEqual(response.status_code, 404)

    def test_get_cars(self):
        response = self.client.get(
            '/cars/',
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)
        cars = json.loads(response.content)
        aston = cars[0]
        golf = cars[1]
        self.assertEqual(self.aston.make, aston["make"])
        self.assertEqual(self.aston.model, aston["model"])
        self.assertEqual(aston["avg_rating"], None)
        self.assertEqual(self.golf.make, golf["make"])
        self.assertEqual(self.golf.model, golf["model"])
        self.assertEqual(golf["avg_rating"], None)

    def test_delete_car_succeeds(self):
        response = self.client.delete(
            '/cars/1/',
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)

    def test_delete_car_fails(self):
        response = self.client.delete(
            '/cars/5/',
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 404)
